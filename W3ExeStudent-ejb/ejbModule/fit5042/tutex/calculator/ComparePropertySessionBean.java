package fit5042.tutex.calculator;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {

	private Set<Property> propertySet;

	public ComparePropertySessionBean() {
		propertySet = new HashSet<>();
	}

	@Override
	public void addProperty(Property searchPropertyById) {
		propertySet.add(searchPropertyById);

	}

	@Override
	public void removeProperty(Property searchPropertyById) {
		for (Property p : propertySet) {
			if (p.getPropertyId() == searchPropertyById.getPropertyId()) {
				propertySet.remove(p);
				break;
			}
		}

	}

	@Override
	public int bestPerRoom() {
		Integer bestRoom = 0;
		int numberOfRooms;
		double price;
		double bestPerRoom = 0;
		for (Property p : propertySet) {
			numberOfRooms = p.getNumberOfBedrooms();
			price = p.getPrice();
			if (bestPerRoom == 0 || price/numberOfRooms < bestPerRoom) {
				bestPerRoom = price/numberOfRooms;
				bestRoom = p.getPropertyId();
			}
		}
		return bestRoom;
	}

}
