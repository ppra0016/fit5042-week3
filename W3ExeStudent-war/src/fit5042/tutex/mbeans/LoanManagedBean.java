/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.component.UIInput;

import fit5042.tutex.calculator.MonthlyPaymentCalculator;
import fit5042.tutex.repository.entities.Loan;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 *          order for students to practice.
 */
@ManagedBean(name = "loanManagedBean", eager = true)
@SessionScoped
public class LoanManagedBean implements Serializable {

	@EJB
	private MonthlyPaymentCalculator calculator;

	private Loan loan;

	public LoanManagedBean() {
		this.loan = new Loan();

	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public String calculate() {

		double monthlyPayment = 0.00;
		// You will need to modify the monthlyPayment value and set it as the monthly
		// payment attribute value into the loan instance
		// Please complete this method starts from here
		monthlyPayment = calculator.calculate(loan.getPrinciple(), loan.getNumberOfYears(), loan.getInterestRate());
		if (monthlyPayment != 0.0) {
			loan.setMonthlyPayment(monthlyPayment);
		}

		return "index";
	}

	public void validatePrinciple(FacesContext context, UIComponent comp, Object value) {

		Double principle = (Double) value;

		if (principle < 1000) {
			((UIInput) comp).setValid(false);

			FacesMessage message = new FacesMessage("Principle must be greater than or equal to 1000");
			context.addMessage(comp.getClientId(context), message);

		}

	}
	
	public void validateNoOfYears(FacesContext context, UIComponent comp, Object value) {

		Integer years = (Integer) value;

		if (years < 1) {
			((UIInput) comp).setValid(false);

			FacesMessage message = new FacesMessage("Number of years must be greater than or equal to 1");
			context.addMessage(comp.getClientId(context), message);

		}

	}
}
